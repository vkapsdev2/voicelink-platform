import Vue from "vue";
import Router from "vue-router";

import HomePage from "../views/user/index";
import LoginPage from "../views/user/Login";
import RegisterPage from "../views/user/Register";

Vue.use(Router);

export const router = new Router({
  mode: "history",
  routes: [
    { path: "/", name: "home", component: HomePage },
    { path: "/user/login", name: "login", component: LoginPage },
    { path: "/user/register", component: RegisterPage },

    // otherwise redirect to home
    { path: "*", redirect: "/" }
  ]
});

router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ["/user/login", "/user/register"];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem("user");

  if (authRequired && !loggedIn) {
    return next("/user/login");
  }
  next();
});
