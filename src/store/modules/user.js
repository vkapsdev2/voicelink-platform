import { Auth } from 'aws-amplify'
import Cookies from 'vue-cookies'
import axios from 'axios'
import { apiUrl } from '@/constants/config'

export default {
  state: {
    currentUser: localStorage.getItem('user') != null ? JSON.parse(localStorage.getItem('user')) : null,
    loginError: null,
    processing: false,
    registerError: null,
    registerSuccess: null
  },
  getters: {
    currentUser: state => state.currentUser,
    processing: state => state.processing,
    loginError: state => state.loginError,
    registerError: state => state.registerError,
    registerSuccess: state => state.registerSuccess

  },
  mutations: {
    setUser (state, payload) {
      state.currentUser = payload
      state.processing = false
      state.loginError = null
    },
    setLogout (state) {
      state.currentUser = null
      state.processing = false
      state.loginError = null
    },
    setProcessing (state, payload) {
      state.processing = payload
      state.loginError = null
    },
    setError (state, payload) {
      state.loginError = payload
      state.currentUser = null
      state.processing = false
    },
    registerError (state, payload) {
      state.registerError = payload
      state.currentUser = null
      state.processing = false
    },
    registerSuccess (state, payload) {
      state.registerSuccess = payload
      state.currentUser = null
      state.processing = false
      state.registerError = null
    },
    clearError (state) {
      state.loginError = null
      state.registerError = null
    }
  },
  actions: {

    login ({ commit }, payload) {
      commit('clearError')
      commit('setProcessing', true)
      try {
        var email = payload.email;
        Auth.signIn(email.toLowerCase(), payload.password).then( async function(user){
            const item = { uid: user.attributes.sub, title: user.attributes['custom:Name'], img: '/assets/img/profile-pic-l.jpg', id: user.attributes.sub, role: user.attributes['custom:role'],is_first: user.attributes['custom:subscription_id'] }
            localStorage.setItem('user', JSON.stringify(item));
            
            commit('setUser', item)
            Cookies.set('email', user.attributes.sub);
            Cookies.set('user', user.attributes['custom:Name']);
            if(user.attributes['custom:subscription_id'] == 'yes'){
              //show Get started notification and update in db for first time user
              await Auth.updateUserAttributes(user, {
                'custom:subscription_id': 'no'
              })
            }else{
              //not a first time user so do nothing
            }
          },
          err => {
            localStorage.removeItem('user')
            commit('setError', err.message)
          }
        )
      } catch (error) {
        console.log(error)
        commit('setError', error)
      }
    },
    signOut ({ commit }) {
      Auth.signOut()
        .then(() => {
          localStorage.removeItem('user')
          commit('setLogout')
          Cookies.remove('email')
        }, _error => {
          console.log(_error)
        })
    },
    register ({ commit }, payload) {
      commit('clearError')
      commit('setProcessing', true)

      let username = payload.email.toLowerCase()
      let password = payload.password
      let name = payload.firstName + ' ' + payload.lastName
      let role = payload.role
      let is_first = 'yes';

      try {
        const signupResponse = Auth.signUp({
          username,
          password,
          attributes: {
            email: username,
            'custom:Name': name,
            'custom:role': role,
            'custom:subscription_id':is_first
          }
        }).then(
          user => {
            console.log('userdata',user.userSub);
            commit('registerSuccess', 'Please check email to verify your link.')
            let config = {
                headers: {
                  "content-type": "application/json",
                  "Access-Control-Allow-Origin":"*"
                }
              }

              let d = new Date();
              let id = d.valueOf();
              id = 'b'+id;

              var h = this;
              var uid = user.userSub;

              const request1 = axios.put(apiUrl+'/bookmarks/',{
                    "id":id+'1',
                    "bookmark_title": "Google",
                    "bookmark_url": "https://google.com",
                    "bookmark_type": "google",
                    "user_id": uid,
                    "search_word": "google"
                  },config)
                  

             const request2 =  axios.put(apiUrl+'/bookmarks/',{
                    "id":id+'2',
                    "bookmark_title": "Google Contact",
                    "bookmark_url": "https://contacts.google.com/",
                    "bookmark_type": "Google Contact",
                    "user_id": uid,
                    "search_word": "google contact"
                  },config)
                  

             const request3 = axios.put(apiUrl+'/bookmarks/',{
                    "id":id+'3',
                    "bookmark_title": "Google Calender",
                    "bookmark_url": "https://calender.google.com",
                    "bookmark_type": "google calender",
                    "user_id": uid,
                    "search_word": "google calender"
                  },config)
                  

              const request4 = axios.put(apiUrl+'/bookmarks/',{
                    "id":id+'4',
                    "bookmark_title": "Google Photos",
                    "bookmark_url": "https://photos.google.com",
                    "bookmark_type": "google photos",
                    "user_id": uid,
                    "search_word": "google photos"
                  },config)
                  
              const request5 = axios.put(apiUrl+'/bookmarks/',{
                    "id":id+'5',
                    "bookmark_title": "My Document",
                    "bookmark_url": "https://docs.google.com/document/d/1MQ23nyjwoXjoHv-_c-lV-lhmxfrGB2n0z5qhyqT7mQQ/edit?usp=sharing",
                    "bookmark_type": "my document",
                    "user_id": uid,
                    "search_word": "my document"
                  },config)
                  

              const request6 = axios.put(apiUrl+'/bookmarks/',{
                    "id":id+'6',
                    "bookmark_title": "Youtube",
                    "bookmark_url": "https://youtube.com",
                    "bookmark_type": "youtube",
                    "user_id": uid,
                    "search_word": "youtube"
                  },config)
                  

                  axios.all([request5, request2, request3, request4, request1, request6]).then(axios.spread((...responses) => {
                    const responseOne = responses[0]
                    const responseTwo = responses[1]
                    const responesThree = responses[2]
                    console.log(responseOne,responseTwo,responesThree)
                    // use/access the results 
                  })).catch(errors => {
                    // react on errors.
                    console.log(errors)
                  });
            
          },
          err => {
            console.log(err)
            commit('registerError', err.message)
          }
        )
        setTimeout(() => {
          // router.replace('/user/login')
          // router.go();
          // this.$router.push({ path: '/user/login' });
        }, 100)
      } catch (error) {
        console.log(error)
        commit('registerError', error)
      }
    },
    getUsers ({ commit }) {
    },
    async UserSubscriptionStatus ({ commit }, payload) {
      let status = payload.is_subscribed
      let subs_id = payload.subs_id

      let user = await Auth.currentAuthenticatedUser({ bypassCache: true }) // Optional, By default is false. If set to true, this call will send a request to Cognito to get the latest user data
      const { attributes } = user
      const userData = JSON.parse(JSON.stringify(attributes))
      if (status) {
        if (userData['custom:subs_id'] != undefined) {
          const subs_array = userData['custom:subs_id'].split(',')
          if (userData['custom:subs_id'].trim() != '' && subs_array.includes(subs_id) != true) {
            subs_id = userData['custom:subs_id'] + ',' + subs_id
          } else {
            if (userData['custom:subs_id'].trim() != '') {
              subs_id = userData['custom:subs_id']
            }
          }
        }
      } else {
        const subs_array = userData['custom:subs_id'].split(',')
        if (userData['custom:subs_id'].trim() != '' && subs_array.includes(subs_id) == true) {
          const index = subs_array.indexOf(subs_id)
          subs_array.splice(index, 1)
          if (subs_array.length > 1) {
            subs_id = subs_array.join(',')
          } else if (subs_array.length == 1) {
            subs_id = subs_array[0]
          } else {
            subs_id = ' '
          }
        } else {
          subs_id = userData['custom:subs_id']
        }
      }
      let result = await Auth.updateUserAttributes(user, {
        'custom:is_subscribed': status.toString(),
        'custom:subs_id': subs_id
      })
    }
  }
}
